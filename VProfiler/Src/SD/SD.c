
#include "SD.h"
#include <stdio.h>
#include <string.h>

FATFS fs;

extern SPI_HandleTypeDef hspi2;
static DSTATUS Stat = STA_NOINIT;
static BYTE CardType;			/* b0:MMC, b1:SDv1, b2:SDv2, b3:Block addressing */

FRESULT initSD()
{
	uint8_t i;
	FRESULT ret = FR_OK;
	for(i = 0; i < 5;++i)
	{
	disk_initialize(0);
	ret = f_mount(&fs, "", 0);
	if(ret == FR_OK) return FR_OK;
	}
	return ret;
}

uint8_t sendByte(uint8_t byte)
{
	uint8_t resp = 0xFF;
	HAL_SPI_TransmitReceive(&hspi2,&byte,&resp,1,1000);
	return resp;
}

uint8_t recieveByte()
{
	return sendByte(0xFF);
}

uint8_t recieveData(uint8_t* buff, uint32_t len)
{
	if(len == 0) return 0;
	uint8_t test;
	do
	{
		test = sendByte(0xFF);//pobudzenie karty SD
		*buff++ = test;
	}
	while(--len);

	return 1;
}

void sendData(const uint8_t* buff, uint32_t len)
{
	if(len == 0) return;

	do  sendByte(*buff++);
	while(--len);
}

int wait_ready (void)
{
	uint32_t i;
	uint8_t test;
	for (i = 500; i ; --i)
	{	//timeout 500ms
		if ((test  = recieveByte()) == 0xFF) break;
		HAL_Delay(1);
	}
	return i ? 1 : 0;
}

void deselect (void)
{
	CS_H();
	recieveByte();//pobudzenie karty sd
}

int _select (void)
{
	CS_L();
	recieveByte();
	if (wait_ready())
		return 1;//gotowa
	deselect();
	return 0;//timeout osiagniety
}

uint8_t recieveDataBlock(uint8_t* buff)
{
	uint8_t d[2];
	uint32_t i;

	for (i = 0; i < 1000; ++i)
	{
		d[0] = recieveByte();
		if (d[0] != 0xFF) break;
		HAL_Delay(1);
	}

	if (d[0] != END_TOKEN) return 0;//zly token

	recieveData(buff, BLOCK_LEN);//pobranie bloku
	recieveData(d, 2);//pomin crc

	return 1;
}

uint8_t sendDataBlock(const uint8_t* buff, uint8_t token)
{
	uint8_t d[2];
	if (!wait_ready()) return 0;

	sendByte(token);
	if (token != STOP_TRAN)
	{
		sendData(buff, 512);	//wyslij strone
		sendData(d, 2);			//wyslij crc
		recieveData(d, 1);		//odbierz odpowiedz
		if ((d[0] & 0x1F) != 0x05)	//niezaakceptowano strony
			return 0;
	}

	return 1;
}

uint8_t sendCmd(uint8_t cmd, uint32_t arg)
{
	int i;

	sendByte(0xFF);//pobudzenie karty
	uint8_t n, d, buf[6];

	if (cmd & 0x80)
	{	//odebrano ACMD<n>
		cmd &= 0x7F;
		n = sendCmd(CMD55, 0);
		if (n > 1) return n;
	}

	buf[0] = 0x40 | cmd;			//komenda sklada sie z indeksu komendy i offsetu 0x40
	buf[1] = (uint8_t)(arg >> 24);	//arg4
	buf[2] = (uint8_t)(arg >> 16);	//arg3
	buf[3] = (uint8_t)(arg >> 8);	//arg2
	buf[4] = (uint8_t)arg;			//arg1
	n = 0x01;						//crc+stop
	if (cmd == CMD0) n = 0x95;		//obliczone crc dla CMD0
	if (cmd == CMD8) n = 0x87;		//crc dla CMD8
	buf[5] = n;						//tylko te 2 komendy wymagaja poprawnego crc w trybie spi
	for(i=0;i<6;i++)
		sendByte(buf[i]);

	if (cmd == CMD12) d = recieveByte();	//pomin odpowiedz dla tej komendy

	n = 100;
	do	//odczekaj na poprawna odpowiedz
		d = recieveByte();
	while ((d & 0x80) && --n);

	return d;
}

DSTATUS disk_status (BYTE drv)
{	//zakladam ze jest tylko 1 FS dla tego systemu
	if (drv) return STA_NOINIT;
	return Stat;
}

//algorytm inicjalizacji SD wedug schematu ze strony
//http://elm-chan.org/docs/mmc/pic/sdinit.png
DSTATUS disk_initialize (BYTE drv)
{
	BYTE n, ty, cmd, buf[4];
	UINT tmr;
	DSTATUS s;
	uint8_t temp = 0;

	uint8_t x = 0xFF;
	if (drv) return RES_NOTRDY;
	HAL_Delay(10);
	CS_H();
	for (n = 10; n; n--)//przestaw karte w stan gotowosci
		HAL_SPI_Transmit(&hspi2,&x,1,99999);
	CS_L();
	ty = 0;
	if (sendCmd(CMD0, 0) == 1) //stan bezczynnosci
	{
		if (sendCmd(CMD8, 0x1AA) == 1) //sprawdz czy karta jest typu SDv2
		{
			recieveData(buf,4);
			if (buf[2] == 0x01 && buf[3] == 0xAA) //karta pracuje na 2.7-3.6V
			{
				for (tmr = 1000; tmr; tmr--) //poczekaj na wyjscie ze stanu bezczynnosci
				{
					temp = sendCmd(ACMD41, 1UL << 30);
					if (temp == 0) break;
					HAL_Delay(1);
				}
				if (tmr && sendCmd(CMD58, 0) == 0) //sprawdz CCS bit w OCR
				{
					recieveData(buf,4);
					ty = (buf[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	// SDv2
				}
			}
		}
		else
		{	//SDv1 czy MMCv3
			if (sendCmd(ACMD41, 0) <= 1)
			{
				ty = CT_SD1; cmd = ACMD41;	// SDv1
			} else
			{
				ty = CT_MMC; cmd = CMD1;	// MMCv3
			}
			for (tmr = 1000; tmr; tmr--)
			{	//odczekaj na opuszczenie stanu bezczynnosci
				if (sendCmd(cmd, 0) == 0) break;
				HAL_Delay(1);
			}
			if (!tmr || sendCmd(CMD16, 512) != 0)	// ustaw wielkosc strony na 512B
				ty = 0;
		}
	}
	CardType = ty;
	s = ty ? 0 : STA_NOINIT;
	Stat = s;
	deselect();

	return s;
}

DRESULT disk_read(BYTE drv,BYTE *buff,DWORD sector,UINT count)
{
	BYTE cmd;
	CS_L();
	sendByte(0xFF);
	if(!wait_ready ()) return RES_ERROR;
	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;
	if (!(CardType & CT_BLOCK)) sector *= 512;	//przekonwertuj LBA na adres

	//czytanie wielu czy pojedynczej strony ?
	cmd = count > 1 ? CMD18 : CMD17;

	if (sendCmd(cmd, sector) == 0)
	{
		do
		{
			if (!recieveDataBlock(buff)) break;
			buff += 512;
		} while (--count);
		if (cmd == CMD18) sendCmd(CMD12, 0);//wyslij koniec transmisji
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}

DRESULT disk_write (BYTE drv,const BYTE *buff,DWORD sector,UINT count)
{
	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;
	if (!(CardType & CT_BLOCK)) sector *= 512;	//przekonwertuj LBA na adres

	CS_L();
	sendByte(0xFF);
	if(!wait_ready ()) return RES_ERROR;
	if (count == 1)
	{	//wysylanie jednej strony
		if ((sendCmd(CMD24, sector) == 0) && sendDataBlock(buff, 0xFE))
			count = 0;
	}
	else
	{	//wysylanie wielu stron
		if (CardType & CT_SDC) sendCmd(ACMD23, count);
		if (sendCmd(CMD25, sector) == 0)
		{
			do
			{
				if (!sendDataBlock(buff, 0xFC)) break;
				buff += 512;
			} while (--count);
			if (!sendDataBlock(0, 0xFD))
				count = 1;
		}
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}

DRESULT disk_ioctl (BYTE drv,BYTE ctrl,void *buff)
{
	DRESULT res;
	BYTE n, csd[16];
	DWORD cs;

	//czy napewno karta zainicjowana ?
	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;

	res = RES_ERROR;
	switch (ctrl)
	{
		case CTRL_SYNC ://sprawdz czy zakonczyl sie zapis
			if (_select()) res = RES_OK;
			break;

		case GET_SECTOR_COUNT :	//sprawdz ilosc sektorow na karcie
			CS_L();
			if ((sendCmd(CMD9, 0) == 0) && recieveData(csd, 16))
			{
				if ((csd[0] >> 6) == 1)
				{	// SDCv2
					cs = csd[9] + ((WORD)csd[8] << 8) + ((DWORD)(csd[7] & 63) << 16) + 1;
					*(DWORD*)buff = cs << 10;
				}
				else
				{	// SDCv1 albo MMC
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					cs = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(DWORD*)buff = cs << (n - 9);
				}
				res = RES_OK;
			}
			break;

		case GET_BLOCK_SIZE :	//wielkosc strony
			*(DWORD*)buff = 128;//zakladam ze juz istnieje FS na karcie sd
			res = RES_OK;		//wiec ta komenda i tak nigdy nie bedzie potrzebna
			break;

		default:
			res = RES_PARERR;
	}

	deselect();

	return res;
}
