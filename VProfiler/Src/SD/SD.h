#ifndef _SD_H
#define _SD_H

#include "stm32f3xx_hal.h"
#include "diskio.h"
#include "ff.h"

extern FATFS fs;

#define CMD0	(0)			/* GO_IDLE_STATE */
#define CMD1	(1)			/* SEND_OP_COND */
#define	ACMD41	(0x80+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(8)			/* SEND_IF_COND */
#define CMD9	(9)			/* SEND_CSD */
#define CMD10	(10)		/* SEND_CID */
#define CMD12	(12)		/* STOP_TRANSMISSION */
#define CMD13	(13)		/* SEND_STATUS */
#define ACMD13	(0x80+13)	/* SD_STATUS (SDC) */
#define CMD16	(16)		/* SET_BLOCKLEN */
#define CMD17	(17)		/* READ_SINGLE_BLOCK */
#define CMD18	(18)		/* READ_MULTIPLE_BLOCK */
#define CMD23	(23)		/* SET_BLOCK_COUNT */
#define	ACMD23	(0x80+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(24)		/* WRITE_BLOCK */
#define CMD25	(25)		/* WRITE_MULTIPLE_BLOCK */
#define CMD32	(32)		/* ERASE_ER_BLK_START */
#define CMD33	(33)		/* ERASE_ER_BLK_END */
#define CMD38	(38)		/* ERASE */
#define CMD55	(55)		/* APP_CMD */
#define CMD58	(58)		/* READ_OCR */

#define CRC_CMD0 0x95
#define CRC_CMD80x1AA 0x87
#define SUCCES_RESPONSE 0x01
#define BLOCK_SIZE 512

#define CS_PORT GPIOC
#define CS_PIN GPIO_PIN_0

#define END_TOKEN 0xFE
#define MULTI_TOKEN 0xFC
#define STOP_TRAN 0xFD

#define BLOCK_LEN 512

#define CS_L()  SET_BIT(GPIOC->BSRR,GPIO_BSRR_BR_0);  // Card Select
#define CS_H() 	SET_BIT(GPIOC->BSRR,GPIO_BSRR_BS_0);  // Card Deselect

extern uint32_t g_Timer2;
extern uint32_t g_Timer1;

typedef struct SD_flags
{
	unsigned int isPoewered:1;
}Sd_flags;

extern Sd_flags g_SD_info;

FRESULT initSD();

uint8_t recieveByte();
uint8_t sendCmd(uint8_t cmd,uint32_t args);
uint8_t sendByte(uint8_t byte);

DRESULT disk_ioctl(BYTE drv,BYTE ctrl,void *buff);
DRESULT disk_write (BYTE drv,const BYTE *buff,DWORD sector,UINT count);
DRESULT disk_read(BYTE drv,BYTE *buff,DWORD sector,UINT count);
DSTATUS disk_initialize (BYTE drv);
DSTATUS disk_status (BYTE drv);
#endif
