#include "PID.h"
#include <stdlib.h>

static double computePID(PID* pid, double SP, double PV)
{
	double error;
	double pResp;
	double dResp;
	double iResp = 0;
	double ret;

	error = SP - PV;

	if (error > pid->maxError)
		pResp = INT16_MAX;
	else if (error < -pid->maxError)
		pResp = -INT16_MAX;
	else
		pResp = pid->P * error;

	if(pid->I != 0.0)
	{
		pid->sumError += error;
		if(pid->sumError > pid->maxSumError)
		{
			iResp = (INT32_MAX / 2);
			pid->sumError = pid->maxSumError;
		}
		else if(pid->sumError < -pid->maxSumError)
		{
			iResp = -(INT32_MAX / 2);
			pid->sumError = -pid->maxSumError;
		}
		else
			iResp = pid->I* pid->sumError;
	}

	dResp = pid->D * (pid->lastPV - PV);

	pid->lastPV = PV;

	ret = (pResp + iResp + dResp);

	if(ret > INT16_MAX)
		ret = INT16_MAX;
	else if(ret < -INT16_MAX)
		ret = -INT16_MAX;

	return ret;
}

void initPID(PID* pid, double P, double I, double D)
{
	pid->compute = computePID;
	pid->sumError = 0.0;
	pid->lastPV = 0.0;
	pid->P = P;
	pid->I = I;
	pid->D = D;
	pid->maxError = INT16_MAX / (P + 1);
	pid->maxSumError = (INT32_MAX / 2) / (I + 1);
}

PID* initNewPID(double P, double I, double D)
{
	PID* newPID = malloc(sizeof(PID));
	initPID(newPID,P,I,D);
	return newPID;
}
