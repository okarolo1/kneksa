
#ifndef PID_PID_H_
#define PID_PID_H_

#include <stdint.h>

struct _pid;
typedef struct _pid PID;
typedef struct _pid
{
	double lastPV;
	double sumError;
	double P,I,D;
	double maxError;
	double maxSumError;

	double (*compute)(PID*, double, double);
}PID;

void initPID(PID* pid, double P, double I, double D);
PID* initNewPID(double P, double I, double D);

#endif
