

#include "TimeFunction.h"
#include <stdlib.h>

static TimeFunction* registeredTimeFunctions[MAX_TIME_FUNCTIONS];
static uint32_t timeFunctionIter = 0;

static void normalizeTFVector()
{
	uint32_t i,j;
	TimeFunction* tF = NULL;
	for(i=0, j=0; i < MAX_TIME_FUNCTIONS; ++i)
	{
		tF = registeredTimeFunctions[i];
		registeredTimeFunctions[i] = NULL;
		if(tF != NULL)
		{
			registeredTimeFunctions[j++] = tF;
		}
	}
}

void registerTimeFunction(void (*f) (void*), void* arg, uint8_t autoRestart, uint64_t ticks, TimerType timerType)
{
	TimeFunction* newTimeFunction = malloc(sizeof(TimeFunction));
	newTimeFunction->autoRestart = autoRestart;
	newTimeFunction->f = f;
	newTimeFunction->ticks = ticks;
	newTimeFunction->arg = arg;
	newTimeFunction->isStatic = 0;
	newTimeFunction->timer = registerTimer(timerType);
	registeredTimeFunctions[timeFunctionIter] = newTimeFunction;
	++timeFunctionIter;
}

void registerStaticTimeFunction(TimeFunction* tF)
{
	tF->timer->resetTimer(tF->timer);
	tF->isStatic = 1;
	registeredTimeFunctions[timeFunctionIter++] = tF;
}

void unregisterTimeFunction(TimeFunction* tF)
{
	uint32_t i = 0;
	TimeFunction* actTF = NULL;
	tF->timer->resetTimer(tF->timer);
	while((actTF = registeredTimeFunctions[i]) != NULL)
	{
		if(actTF == tF)
		{
			if(!tF->isStatic)
				free(tF);
			--timeFunctionIter;
			registeredTimeFunctions[i] = NULL;
			break;
		}
		++i;
	}
	normalizeTFVector();
}


void updateTimeFunctions()
{
	uint32_t i = 0;
	TimeFunction* tF = NULL;
	while((tF = registeredTimeFunctions[i]) != NULL)
	{
		if(tF->timer->getElapsedTime(tF->timer) >= tF->ticks)
		{
			tF->f(tF->arg);
			if(!tF->autoRestart)
			{
				if(!tF->isStatic)
					free(tF);
				--timeFunctionIter;
				registeredTimeFunctions[i] = NULL;
			}
		}
		++i;
	}

	normalizeTFVector();
}








