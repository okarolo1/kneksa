#include "VelocityProfiler.h"
#include "../SD/SD.h"
#include <stdlib.h>
#include <math.h>

/**************** PRIVATE ***************/
static uint8_t _dcProfilerIter = 0;
static VelocityProfiler* registeredDCProfilers[MAX_DC_PROILERS];
static void intChangeFreq(VelocityProfiler* prof);
uint8_t makeStepperStep(VelocityProfiler* prof);
static uint8_t stepperShouldStopping(VelocityProfiler* prof);

/**** DC MOTOR ****/

static uint8_t DCshouldStopping(VelocityProfiler* prof);
static void getActualDCMotorVelocity(VelocityProfiler* prof);
static void makeDCMotorStep(VelocityProfiler* prof);
static void DCwakeup(void* prf);
static void updateDCProfilers();
/**************** PRIVATE ***************/



/*************** CONFIGURE **************/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM17)
	{
		updateHighFreqTimers();
		updateTimeFunctions();
	}
	else if(htim->Instance == TIM6)
	{
		testProfiler.update(&testProfiler);
	}
}

void HAL_SYSTICK_Callback()
{
	updateLowFreqTimers();
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_3)
		updateDCProfilers();
}

static void stepperStep(MotorID ID,Direction dir)
{
	switch(ID)
	{
	case MOTOR2:
		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_SET);
		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_RESET);
		break;
	default:
		break;
	}
}

static uint8_t stepperLostStep(VelocityProfiler* prof)
{
	return 0;
}

/**** DC MOTOR ****/

static void changePWM(TIM_HandleTypeDef* htim,MotorID ID,double PV)
{
	switch(ID)
	{
	case MOTOR1:
		htim->Instance->CCR1 = PV;
		break;
	default:
		break;
	}
}

static void startPWM(TIM_HandleTypeDef* htim, MotorID ID)
{
	switch(ID)
	{
	case MOTOR1:
		HAL_TIM_PWM_Start(htim,TIM_CHANNEL_1);
		break;
	default:
		break;
	}
}

static void setForward(MotorID ID)
{
	switch(ID)
	{
	case MOTOR1:
		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_RESET);
		break;
	default:
		break;
	}
}

static void setBack(MotorID ID)
{
	switch(ID)
	{
	case MOTOR1:
		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_1,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_2,GPIO_PIN_SET);
		break;
	default:
		break;
	}
}
/*************** CONFIGURE **************/



/*************** DEBUGGING **************/
#ifdef DEBUG_VELOCITY_PROFILER
VelocityProfiler testProfiler;
volatile int32_t actsp[1000];
volatile uint64_t t[1000];
volatile int32_t step[1000];
volatile int32_t veloc[1000];
volatile int16_t ccr[1000];
volatile uint16_t dbg_it = 0;

void startDebugging(EngineType type)
{
	if(type == DCMOTOR)
	{
		initVelocityProfilerDCMotor(&testProfiler,230,8,20,&htim2,MOTOR1);
		testProfiler.minPIDResp = 105;
	}
	else
	{
		initVelocityProfilerStepper(&testProfiler,200,5,5,&htim6,MOTOR2);
	}
	testProfiler.moveSteps(&testProfiler, 500);
}

void saveToSD()
{
	int i;
	FIL fil;
	f_open(&fil, "expet.txt",  FA_OPEN_ALWAYS | FA_OPEN_APPEND | FA_WRITE);
	UINT bw;
	char number[20]; memset(number,0x00,20);
	for(i=0; i < 800; ++i)
	{
		itoa(actsp[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(step[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(veloc[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(ccr[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_write(&fil, "\n\r", strlen("\r\n"), &bw);
	for(i=0; i < 800; ++i)
	{
		itoa(t[i],number,10);
		f_write(&fil, number, strlen(number), &bw);
		f_write(&fil, ", ", strlen(", "), &bw);
	}
	f_close(&fil);
}

void updateDebugInfo()
{
	VelocityProfiler* prof = &testProfiler;
	if(dbg_it > 980) return;
	t[dbg_it] = t[dbg_it-1]+ prof->tim->getElapsedTime(prof->tim);
	actsp[dbg_it] = prof->actSetPoint;
	veloc[dbg_it] = prof->actualVelocity;
	ccr[dbg_it] = prof->htim->Instance->CCR1;
	step[dbg_it++] = prof->actualStep;
}
#else

void saveToSD(){}
void updateDebugInfo(){}

void startDebugging(EngineType type)
{
	(void)type;
}
#endif
/*************** DEBUGGING **************/



/***************  PRIVATE STEPPER  **************/
static void intChangeFreq(VelocityProfiler* prof)
{
	if(prof->state == STOPPED)
		HAL_TIM_Base_Stop_IT(prof->htim);
	uint32_t prescaler = BASE_FREQ / fabs(prof->actSetPoint);
	prof->htim->Instance->ARR = prescaler;
}

uint8_t makeStepperStep(VelocityProfiler* prof)
{
	if(prof->actSetPoint < 0)
	{
		--(prof->actualStep);
		stepperStep(prof->motorID,BACK);
		if(stepperLostStep(prof))
		{
			++(prof->actualStep);
			return 0;
		}
	}
	else
	{
		++(prof->actualStep);
		stepperStep(prof->motorID,FORWARD);
		if(stepperLostStep(prof))
		{
			--(prof->actualStep);
			return 0;
		}
	}
	return 1;
}

static uint8_t stepperShouldStopping(VelocityProfiler* prof)
{
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;

	if((prof->actSetPoint > 0 && dir == BACK) || (prof->actSetPoint < 0 && dir == FORWARD))
		return 0;

	int32_t leftSteps = prof->destStep - prof->actualStep;
	double vNext = leftSteps < 0 ? -prof->minVelocity : prof->minVelocity;

	leftSteps = abs(leftSteps)-1;
	int32_t S = ((prof->actSetPoint - vNext)*(prof->actSetPoint + vNext))/(2*prof->acc);
	if(S >= leftSteps)
		return 1;
	return 0;
}
/***************  PRIVATE STEPPER  **************/



/***************  PUBLIC STEPPER  **************/

static uint8_t stepperMoveSteps(VelocityProfiler* prof, int32_t destStep)
{
	 prof->tim->resetTimer(prof->tim);
	prof->destStep = destStep;

	if(prof->state == STOPPED)
	{
		prof->actualVelocity = 0;
		prof->tim->resetTimer(prof->tim);

		if(prof->actualStep < destStep )
			prof->actSetPoint = prof->minVelocity;
		else
			prof->actSetPoint = -prof->minVelocity;
	}

	makeStepperStep(prof);
	if(prof->state == STOPPED)
	{
		HAL_TIM_Base_Start_IT(prof->htim);
		prof->state = RUNNING;
		intChangeFreq(prof);
	}
	else
		prof->state = RUNNING;

	updateDebugInfo();
	return 1;
}

static void stepperUpdate(VelocityProfiler* prof)
{
	if(prof->state != STOPPED)
	{
		Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
		if((prof->destStep == prof->actualStep) && (prof->actSetPoint < 0))
			dir = BACK;
		uint8_t shouldStopping = stepperShouldStopping(prof);
		double velocitQuantum = fabs(prof->acc/prof->actSetPoint);

		if(shouldStopping && dir == FORWARD)
			velocitQuantum *= -1;
		else if(!shouldStopping && dir == BACK)
			velocitQuantum *= -1;

		prof->actSetPoint += velocitQuantum;

		//freq overflow ?
		if(prof->actSetPoint >= prof->maxVelocity)
			prof->actSetPoint = prof->maxVelocity;
		if(prof->actSetPoint <= -prof->maxVelocity)
			prof->actSetPoint = -prof->maxVelocity;

		if((prof->destStep == prof->actualStep) && (fabs(prof->actSetPoint) <= fabs(prof->minVelocity*1.5)))
		{
			prof->state = STOPPED;
			prof->actSetPoint = 0;
			intChangeFreq(prof);
			return;
		}

		if(!makeStepperStep(prof))
			prof->actSetPoint -= velocitQuantum;

		intChangeFreq(prof);
		updateDebugInfo();
	}
}

void initVelocityProfilerStepper(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity ,uint32_t _acc, TIM_HandleTypeDef* htim, MotorID motorID)
{
	memset(prof, 0x00 ,sizeof(VelocityProfiler));
	prof->state = STOPPED;
	prof->maxVelocity = _maxVelocity;
	prof->acc = _acc;
	prof->htim = htim;
	prof->minVelocity = minVelocity;
	prof->moveSteps = stepperMoveSteps;
	prof->update = stepperUpdate;
	prof->motorID = motorID;

#ifdef DEBUG_VELOCITY_PROFILER
	prof->tim = registerTimer(HIGHFREQ);
#endif
}
/***************  PUBLIC STEPPER  **************/



/***************  PRIVATE DC MOTOR **************/
static uint8_t DCshouldStopping(VelocityProfiler* prof)
{
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
	if((prof->actSetPoint > 0 && dir == BACK) || (prof->actSetPoint < 0 && dir == FORWARD))
		return 0;

	int32_t leftSteps = prof->destStep - prof->actualStep;
	double vNext = leftSteps < 0 ? -prof->minVelocity : prof->minVelocity;

	leftSteps = abs(leftSteps)-1;
	int32_t S = ((prof->actualVelocity - vNext)*(prof->actualVelocity + vNext))/(prof->acc);
	if(S >= leftSteps)
		return 1;
	return 0;
}

static void getActualDCMotorVelocity(VelocityProfiler* prof)
{
	if(prof->tim->overlfow)
		prof->actualVelocity = 0.0;

	updateDebugInfo();
	double elapsedTime = prof->tim->getElapsedTime(prof->tim);
	if(fabs(prof->actualVelocity) > prof->maxVelocity*5)
	{
		prof->actualVelocity = (BASE_FREQ/elapsedTime);
	}
	else
		prof->actualVelocity = 0.7*(BASE_FREQ/elapsedTime) + 0.3*(fabs(prof->actualVelocity));
	if(prof->actSetPoint < 0)
		prof->actualVelocity *= -1;
	prof->tim->resetTimer(prof->tim);
}

static void makeDCMotorStep(VelocityProfiler* prof)
{

	if(prof->actSetPoint > 0)
		setForward(prof->motorID);
	else
		setBack(prof->motorID);

	double PIDResponse = prof->pid->compute(prof->pid,fabs(prof->actSetPoint), fabs(prof->actualVelocity));

	if(PIDResponse < prof->minPIDResp)
		PIDResponse = prof->minPIDResp;

	if(PIDResponse > PWM_MAX)
		PIDResponse= PWM_MAX;

	changePWM(prof->htim, prof->motorID ,PIDResponse);
}

static void DCwakeup(void* prf)
{
	VelocityProfiler* prof = prf;
	Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
	(dir == FORWARD) ? (prof->actSetPoint = prof->minVelocity) : (prof->actSetPoint = -prof->minVelocity);
	makeDCMotorStep(prof);
	prof->state = RUNNING;
	prof->actualVelocity = 0;

	if(prof->actualStep < prof->destStep )
		prof->actSetPoint = prof->minVelocity;
	else
		prof->actSetPoint = -prof->minVelocity;

	if(prof->actSetPoint > 0)
		setForward(prof->motorID);
	else
		setBack(prof->motorID);

	changePWM(prof->htim, prof->motorID ,prof->minPIDResp);
	updateDebugInfo();

	prof->tim->resetTimer(prof->tim);
	registerStaticTimeFunction(prof->DCwatchdog);
}

static void DCwakeRunning(void* prf)
{
	VelocityProfiler* prof = prf;
	if(prof->state == RUNNING)
	{
		prof->actSetPoint = 0;
		prof->pid->lastPV = 0;

		uint32_t PWM = prof->htim->Instance->CCR1 += 20;
		if(PWM > PWM_MAX)
			PWM= PWM_MAX;

		prof->htim->Instance->CCR1 = PWM;
	}
	prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
	prof->tim->resetTimer(prof->tim);
}

static void updateDCProfilers()
{
	uint8_t i;
	for(i = 0; i < _dcProfilerIter; ++i)
	{
		VelocityProfiler* prof = registeredDCProfilers[i];
		getActualDCMotorVelocity(prof);
		if(prof->state == STOPPED || prof->state == SUSPENDED)
		{
			if(prof->velocityBeforeStop > 0)
				prof->actualStep++;
			else
				prof->actualStep--;
		}
		else
		{
			if(prof->actSetPoint > 0)
				prof->actualStep++;
			else
				prof->actualStep--;
		}
		if(prof->state != SUSPENDED)
		{
			prof->update(registeredDCProfilers[i]);
		}
	}
}
/***************  PRIVATE DC MOTOR **************/



/***************  PUBLIC DC MOTOR **************/
static uint8_t DCMoveSteps(VelocityProfiler* prof, int32_t destStep)
{
	prof->DCwatchdog->autoRestart = 1;
	prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
	registerStaticTimeFunction(prof->DCwatchdog);

	prof->destStep = destStep;
	if(prof->state == STOPPED)
	{
		prof->actualVelocity = 0;
		prof->tim->resetTimer(prof->tim);
		if(prof->actualStep < destStep )
			prof->actSetPoint = prof->minVelocity;
		else
			prof->actSetPoint = -prof->minVelocity;
	}
	else getActualDCMotorVelocity(prof);

	if(prof->actSetPoint > 0)
		setForward(prof->motorID);
	else
		setBack(prof->motorID);

	changePWM(prof->htim, prof->motorID ,prof->minPIDResp);
	prof->state = RUNNING;
	return 1;
}

static void DCupdate(VelocityProfiler* prof)
{
	if(prof->state != STOPPED)
	{
		prof->DCwatchdog->timer->resetTimer(prof->DCwatchdog->timer);
		//if(prof->actualStep == 300)
		//	prof->destStep = 100;
		Direction dir = prof->destStep > prof->actualStep ? FORWARD: BACK;
		if((prof->destStep == prof->actualStep) && (prof->actSetPoint < 0))
			dir = BACK;

		uint8_t shouldStopping = DCshouldStopping(prof);
		double velocitQuantum = fabs(prof->acc/prof->actualVelocity);

		if(shouldStopping && dir == FORWARD)
			velocitQuantum *= -1;
		else if(!shouldStopping && dir == BACK)
			velocitQuantum *= -1;

		prof->actSetPoint += velocitQuantum;

		//freq overflow ?
		if(prof->actSetPoint >= prof->maxVelocity)
			prof->actSetPoint = prof->maxVelocity;
		if(prof->actSetPoint <= -prof->maxVelocity)
			prof->actSetPoint = -prof->maxVelocity;

		if((prof->destStep == prof->actualStep) && (fabs(prof->actSetPoint) <= fabs(prof->minVelocity*1.5)))
		{
			prof->velocityBeforeStop = prof->actualVelocity;
			prof->state = STOPPED;
			prof->actSetPoint = 0;
			changePWM(prof->htim,prof->motorID, 0);
			unregisterTimeFunction(prof->DCwatchdog);
			return;
		}

		uint8_t SPUnderMin = fabs(prof->actSetPoint) < prof->minVelocity;
		if( (prof->actSetPoint >= 0 && SPUnderMin && dir == BACK) || (prof->actSetPoint <= 0 && SPUnderMin && dir == FORWARD))
		{
			unregisterTimeFunction(prof->DCwatchdog);
			prof->velocityBeforeStop = prof->actualVelocity;
			prof->state = SUSPENDED;
			prof->actSetPoint = 0;
			changePWM(prof->htim,prof->motorID, 0);
			prof->pid->lastPV = 0;
			registerStaticTimeFunction(prof->wakeSuspendedF);
			return;
		}

		makeDCMotorStep(prof);
	}
}

void initVelocityProfilerDCMotor(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc,TIM_HandleTypeDef* htim, MotorID motorID)
{
	memset(prof, 0x00 ,sizeof(VelocityProfiler));
	prof->state = STOPPED;
	prof->maxVelocity = _maxVelocity;
	prof->acc = _acc;
	prof->minVelocity = minVelocity;
	prof->motorID = motorID;
	prof->htim = htim;
	prof->moveSteps = DCMoveSteps;
	prof->update = DCupdate;

	prof->tim = registerTimer(HIGHFREQ);
	prof->pid = initNewPID(50,0,50);
	prof->pid->lastPV = prof->minPIDResp;
	startPWM(htim,motorID);
	htim->Instance->CCR1 = 0;

	TimeFunction* tF = malloc(sizeof(TimeFunction));
	prof->wakeSuspendedF = tF;
	tF->arg = prof;
	tF->autoRestart = 0;
	tF->f = DCwakeup;
	tF->isStatic = 1;
	tF->ticks = 500;
	tF->timer = registerTimer(LOWFREQ);

	TimeFunction* tF2 = malloc(sizeof(TimeFunction));
	prof->DCwatchdog = tF2;
	tF2->arg = prof;
	tF2->autoRestart = 0;
	tF2->f = DCwakeRunning;
	tF2->isStatic = 1;
	tF2->ticks = 100;
	tF2->timer = registerTimer(LOWFREQ);

	registeredDCProfilers[_dcProfilerIter++] = prof;
}
/***************  PUBLIC DC MOTOR **************/

















