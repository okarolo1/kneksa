#ifndef _VELOCITYPROFILER_H
#define _VELOCITYPROFILER_H
#include <stdint.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "../Settings.h"
#include "../Timer/Timer.h"
#include "../PID/PID.h"
#include "../TimeFunction/TimeFunction.h"

#define BASE_FREQ 40000.0		//HIGH timer freq
#define BASE_PWM_FREQ 20000		//PWM freq
#define MAX_DC_PROILERS 5
#define PWM_MAX 1600

#define FORWARD 1
#define BACK (-1)
typedef int8_t Direction;

typedef enum
{
	MOTOR1,
	MOTOR2,
	MOTOR_LAST
}MotorID;

typedef enum
{
	RUNNING,
	STOPPING,
	SUSPENDED,
	STOPPED
}ProfilerState;

typedef enum
{
	STEPPER,
	DCMOTOR
} EngineType;

struct _velocityProfiler;
typedef struct _velocityProfiler VelocityProfiler;
struct _velocityProfiler
{
	MotorID motorID;
	volatile ProfilerState state;
	volatile int32_t destStep;			//destination step number
	volatile int32_t actualStep;		//actual step
	volatile double actSetPoint;		//steps/s
	volatile double velocityBeforeStop;		//steps/s

	double maxVelocity;					//steps/sec
	double minVelocity;					//steps/sec
	double minPIDResp;
	double actualVelocity;
	uint32_t acc;						//steps/s+^2
	TIM_HandleTypeDef* htim;			//freq timer (for stepper motors)
	Timer* tim;							//timer for debugging or for measuring velocity
	PID* pid;
	TimeFunction* wakeSuspendedF;		//for DC motors to wakeup suspended motor
	TimeFunction* DCwatchdog;			//for DC motors to wakeup RUNNING motor
	uint8_t (*moveSteps) (VelocityProfiler*, int32_t);			//run engine
	void (*update) (VelocityProfiler* prof);
};

void initVelocityProfilerStepper(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc, TIM_HandleTypeDef* htim, MotorID motorID);
void initVelocityProfilerDCMotor(VelocityProfiler* prof, uint32_t _maxVelocity, uint32_t minVelocity, uint32_t _acc,TIM_HandleTypeDef* htim,MotorID motorID);


extern VelocityProfiler testProfiler;
/*************** DEBUGGING **************/
#ifdef DEBUG_VELOCITY_PROFILER
#include "../SD/SD.h"
#include "tim.h"
extern VelocityProfiler testProfiler;
extern volatile int32_t actsp[1000];
extern volatile uint64_t t[1000];
extern volatile int32_t step[1000];
extern volatile int32_t veloc[1000];
extern volatile int16_t ccr[1000];
extern volatile uint16_t dbg_it;
#endif
void startDebugging(EngineType type);
void saveToSD();
void updateDebugInfo();
/*************** DEBUGGING **************/

#endif
