#include "Timer.h"
#include <stdlib.h>

static uint8_t _lowFreqTimerCounter = 0;
static uint8_t _highFreqTimerCounter = 0;
static Timer* registeredHTimers[MAX_HFREQ_TIMERS];
static Timer* registeredLTimers[MAX_LFREQ_TIMERS];

uint64_t getElapsedTime(Timer* tim)
{
	return tim->tick;
}

uint64_t resetTimer(Timer* tim)
{
	uint64_t tick = tim->tick;
	tim->overlfow = 0;
	tim->tick = 0;
	return tick;
}

Timer* registerTimer(TimerType type)
{
	Timer* tim = malloc(sizeof(Timer));
	tim->tick = 0;
	tim->getElapsedTime = getElapsedTime;
	tim->resetTimer = resetTimer;
	tim->overlfow = 0;

	if(type == LOWFREQ)
	{
		if(_lowFreqTimerCounter == MAX_LFREQ_TIMERS)
			return 0;

		registeredLTimers[_lowFreqTimerCounter] = tim;
		++_lowFreqTimerCounter;
	}
	else
	{
		if(_highFreqTimerCounter == MAX_HFREQ_TIMERS)
			return 0;

		registeredHTimers[_highFreqTimerCounter] = tim;
		++_highFreqTimerCounter;
	}
	return tim;
}

void updateHighFreqTimers()
{
	uint8_t i;
	for(i = 0; i < _highFreqTimerCounter; ++i)
	{
		if(registeredHTimers[i]->tick == UINT64_MAX)
			registeredHTimers[i]->overlfow = 1;
		registeredHTimers[i]->tick +=1;
	}
}

void updateLowFreqTimers()
{
	uint8_t i;
	for(i = 0; i < _lowFreqTimerCounter; ++i)
	{
		if(registeredLTimers[i]->tick == UINT64_MAX)
			registeredLTimers[i]->overlfow = 1;
		registeredLTimers[i]->tick +=1;
	}
}
